import h2d.ScaleGrid;
import h2d.Layers;

class Main extends hxd.App {
	public static function main() {
		#if js
		hxd.Res.initEmbed();
		#else
		hxd.Res.initLocal();
		#end
		new Main();
	}

	override function init() {
		engine.backgroundColor = 0xb4e5e7;
		s2d.scaleMode = LetterBox(960, 600);

		var section = new h2d.ScaleGrid(hxd.Res.tiles.get("buy_background"), 4, 1, 1, 1, s2d);
		section.width = 120;
		section.height = 600;
		section.setPosition(960 - section.width, 0);
		var buy_text = new h2d.Bitmap(hxd.Res.tiles.get("buy_text"), s2d);
		buy_text.setPosition(section.x + section.width / 2 - buy_text.tile.width / 2, 20);

		layers = new Layers(s2d);
		var sacrifice = new Card.Sacrifice(layers);
		sacrifice.setPosition(20, 600 - 97 - 20);
		final y = 50;
		final sep = 107;
		(new Card.Buy(1, "cake", Card.Cake,  layers)).setPos(section.x + section.width / 2 - 60 / 2, y + sep * 0);
		(new Card.Buy(2, "cleaner", Card.Cleaner,  layers)).setPos(section.x + section.width / 2 - 60 / 2, y + sep * 1);
		(new Card.Buy(2, "maintenance", Card.Maintenance,  layers)).setPos(section.x + section.width / 2 - 60 / 2, y + sep * 2);
		(new Card.Buy(3, "worker", Card.Worker,  layers)).setPos(section.x + section.width / 2 - 60 / 2, y + sep * 3);
		(new Card.Buy(4, "production", Card.Production,  layers)).setPos(section.x + section.width / 2 - 60 / 2, y + sep * 4);

		(new Card.Production(layers)).setPosition(85, 35);
		(new Card.Production(layers)).setPosition(165, 35);
		(new Card.Production(layers)).setPosition(245, 35);
		for (i in 0...2) {
			(new Card.Worker(layers)).setPosition(300, 350 + 20 * i);
		}
		for (i in 0...1) {
			(new Card.Cleaner(layers)).setPosition(380, 350 + 20 * i);
		}
		for (i in 0...1) {
			(new Card.Maintenance(layers)).setPosition(460, 350 + 20 * i);
		}
		sacrifice.dropCoins(540, 350, 4);
		hxd.Res.sadday.play(true, 0.25);

	}

	override function update(dt: Float) {
		for(card in cards) {
			card.update(dt);
		}
		next -= dt;
		if (next <= 0) {
			next += Math.random() * 10 + 5;
			var r = Math.random();
			var card = if (r > 0.70) {
				new Card.Boss(layers);
			} else if (r > 0.35) {
				new Card.Leak(layers);
	 		} else {
				new Card.Spill(layers);
			}
			card.setPos(Math.random() * (960 - 120 - 180) + 60, Math.random() * (600 - 291) + 60);
		}
	}

	var layers:h2d.Layers;
	var next = 5.0;
	public static final cards = new Array<Card>();
	public static var performance = 0;
}
import h2d.Object;
import h2d.col.Bounds;
import h2d.Bitmap;

class Card extends h2d.Bitmap {
	public function new(tile:h2d.Tile, layers:h2d.Layers) {
		super(tile);
		this.layers = layers;
		layers.add(this, 0);
		Main.cards.push(this);
	}

	public function update(dt:Float) { }

	public function onWorkDone() {}

	public function onCardRemoved(card:Card) {}

	public function onCardAdded(card:Card) {}

	public function setPos(x:Float, y:Float) {
		setPosition(hxd.Math.clamp(x, 0, 960 - tile.width), hxd.Math.clamp(y, 0, 600 - tile.height));
	}

	public function reparent(obj:h2d.Object) {
		setPosition(absX - obj.absX, absY - obj.absY);
		try {
			cast(parent, Card).onCardRemoved(this);
		} catch (e) {
			// *shrug*
		}
		remove();
		parent = obj;
		obj.addChild(this);
		try {
			cast(obj, Card).onCardAdded(this);
		} catch (e) {
			// *shrug*
		}
	}

	private function removeSelf() {
		for (c in children) {
			try {
				cast(c, Card).reparent(layers);
			} catch (e) {
				// *shrug*
			}
		}
		remove();
		Main.cards.remove(this);
	}

	final layers:h2d.Layers;
}

class ReverseArrayIterator<T> {
	final arr:Array<T>;
	var i:Int;

	public inline function new(arr:Array<T>) {
		this.arr = arr;
		this.i = this.arr.length - 1;
	}

	public inline function hasNext()
		return i > -1;

	public inline function next() {
		return arr[i--];
	}

	public static inline function reversedValues<T>(arr:Array<T>) {
		return new ReverseArrayIterator(arr);
	}
}

class DragCard extends Card {
	public function new(tile:h2d.Tile, layers:h2d.Layers) {
		super(tile, layers);
		interaction = new h2d.Interactive(tile.width, tile.height, this);
		interaction.onPush = startDrag;
	}

	private function startDrag(e:hxd.Event) {
		final offset = {x: e.relX, y: e.relY};
		reparent(layers);
		interaction.cursor = hxd.Cursor.Move;
		interaction.startCapture(function(e:hxd.Event) {
			switch (e.kind) {
				case ERelease:
					release();
				case EMove:
					e.propagate = false;
					setPos(x + e.relX - offset.x, y + e.relY - offset.y);
					layers.ysort(0);
				case _:
			}
		});
	}

	private function release() {
		interaction.stopCapture();
		interaction.cursor = hxd.Cursor.Button;
		getBounds(layers, bounds);
		checkCollisions([for (obj in layers.getLayer(0)) obj]);
	}

	private function checkCollisions(objects:Array<h2d.Object>):Bool {
		for (obj in new ReverseArrayIterator(objects)) {
			if (checkCollisions(obj.children)) {
				return true;
			}
			if (obj == this) {
				continue;
			}
			if (!Std.isOfType(obj, Card)) {
				continue;
			}
			if (obj.absY > absY) {
				continue;
			}
			obj.getBounds(layers, otherBounds);
			if (otherBounds.intersects(bounds)) {
				reparent(obj);
				return true;
			}
		}
		return false;
	}


	final interaction:h2d.Interactive;
	final bounds = new h2d.col.Bounds();
	final otherBounds = new h2d.col.Bounds();
}

class WorkCard extends DragCard {

	public function new(tile:h2d.Tile, layers:h2d.Layers) {
		super(tile, layers);
		background = new h2d.Bitmap(hxd.Res.tiles.get("progress_background"), this);
		background.setPosition(0, -10);
		background.visible = false;
		fill = new h2d.ScaleGrid(hxd.Res.tiles.get("progress_fill"), 1, 1, 1, background);
		fill.setPosition(2, 2);
		fill.width = 0;
		fill.height = 4;
		progress = 0;
	}

	override function update(dt: Float) {
		progress += dt * work * 0.25;
		if (progress >= 1) {
			progress -= 1;
			onWorkDone();
		}
		fill.width = progress * 54;
	}

	override function onCardRemoved(card: Card) {
		work = 0;
	}

	final background:h2d.Bitmap;
	final fill:h2d.ScaleGrid;
	var progress:Float;
	var work:Float = 0.0;
}

class Coin extends DragCard {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("coin"), layers);
	}
}

class Worker extends DragCard {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("worker"), layers);
	}
}

class Cleaner extends DragCard {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("cleaner"), layers);
	}
}

class Cake extends DragCard {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("cake"), layers);
	}
}

class Maintenance extends DragCard {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("maintenance"), layers);
	}
}

class Boss extends WorkCard {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("boss"), layers);
		Main.performance++;
	}

	override function onCardAdded(card:Card) {
		switch (Type.getClass(card)) {
			case Cake:
				background.visible = true;
				work = 1 * 0.4;
		}
	}

	override function onWorkDone() {
		Main.performance--;
		remove();
		Main.cards.remove(this);
	}
}

class Production extends WorkCard {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("production"), layers);
	}

	override function onWorkDone() {
		(new Coin(layers)).setPos(absX + Math.random() * 100 - 50, absY + Math.random() * 50);
	}

	override function update(dt:Float) {
		final oldWork = work;
		if (Main.performance > 0) {
			work /= 2;
		}
		super.update(dt);
		work = oldWork;
	}

	override function onCardAdded(card:Card) {
		switch (Type.getClass(card)) {
			case Worker:
				background.visible = true;
				work = 1 * 0.25;
			case Cleaner | Maintenance:
				background.visible = true;
				work = 0.5 * 0.25;
		}
	}
}

class Spill extends WorkCard {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("spill"), layers);
		Main.performance++;
	}

	override function onCardAdded(card:Card) {
		switch (Type.getClass(card)) {
			case Cleaner:
				background.visible = true;
				work = 1 * 0.4;
			case Worker | Maintenance:
				background.visible = true;
				work = 0.75 * 0.4;
		}
	}

	override function onWorkDone() {
		Main.performance--;
		removeSelf();
	}
}

class Leak extends WorkCard {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("leak"), layers);
		Main.performance++;
	}

	override function onCardAdded(card:Card) {
		switch (Type.getClass(card)) {
			case Maintenance:
				background.visible = true;
				work = 1 * 0.5;
			case Worker | Cleaner:
				background.visible = true;
				work = 0.75 * 0.5;
		}
	}

	override function onWorkDone() {
		Main.performance--;
		removeSelf();
	}
}

class Sacrifice extends Card {
	public function new(layers:h2d.Layers) {
		super(hxd.Res.tiles.get("sacrifice"), layers);
	}

	override function onCardAdded(card:Card) {
		switch (Type.getClass(card)) {
			case Maintenance|Cleaner|Cake:
				dropCoins(card.absX + 10, card.absY - 10, 1);
				card.removeSelf();
			case Worker:
				dropCoins(card.absX + 10, card.absY - 10, 2);
				card.removeSelf();
			case Production:
				dropCoins(card.absX + 10, card.absY - 10, 3);
				card.removeSelf();
		}
	}

	public function dropCoins(x: Float, y: Float, count: Int) {
		var coin = null;
		for (i in 0...count) {
			final prev = coin;
			coin = new Card.Coin(layers);
			coin.setPosition(x, y + 20 * i);
			coin.getAbsPos();
			if (prev != null) {
				coin.reparent(prev);
			}
		}
	}
}

class Buy extends Card {
	public function new(price: Int, what: String, type: Class<Card>, layers:h2d.Layers) {
		super(hxd.Res.tiles.get("buy_" + what), layers);
		this.price = price;
		this.type = type;
	}

	override function onCardAdded(card:Card) {
		final coins = [];
		countCoins([card], coins);
		if (coins.length >= price) {
			for(i in 0...price) {
				coins[i].removeSelf();
			}
		var newCard = Type.createInstance(type, [layers]);
		newCard.setPos(absX - 20, absY - 10);
		}
	}

	private function countCoins(cards:Array<Object>, coins:Array<Card.Coin>) {
		for(card in cards) {
		try {
				coins.push(cast(card, Card.Coin));
		} catch (e) {
			// *shrug*
		}
		countCoins(card.children, coins);
		}
	}

	final price: Int;
	final type: Class<Card>;
}